# Tanzania Use-Case

![tanzania](images/tanzania.jpg)

## Part 1 - Notebooks

## Instructions to re-run notebooks

- [tensorflow docker image](https://hub.docker.com/r/tensorflow/tensorflow/) was used to run the notebooks, e.g.

```
docker run -it --rm -v $(pwd):/tf/notebooks -p 8888:8888 tensorflow/tensorflow:latest-py3-jupyter
```

The provided `requirements.txt` within `notebooks` contains the requirements, which are installed in the first cells.

- Plotting the interactive map takes time (<5 minutes), an export has been made in [tanzania_map.html](notebooks/tanzania_map.html) (30MB)
- Generating the pandas profile takes time (<5 minutes),
an export has been made in [profiling_tanzania.html](notebooks/profiling_tanzania.html)

---


### 1. Data Exploration

[01 - Data Exploration.ipynb](notebooks/01 - Data Exploration.ipynb) contains the data exploration notebook

![folium](images/folium.gif)

#### Key Takeaways

- Pumps seem to be surrounded with the same class, i.e. working pumps clustered together, none working together 
- 'Dry' quality seems a good predictor in quality column
- KNN seems not a weird idea, looking at the map

* The map is saved in `.html`
* The Pandas Profiling report is saved in `.html`

![dabl](images/dabl.gif)

### 2. Feature Engineering & Classification

[02 - Feature Engineering & Classification.ipynb](notebooks/02 - Feature Engineering & Classification.ipynb) contains the modeling notebook

![confusion_matrix](images/confusion_matrix.png)

---

![features](images/features.png)


#### Key Takeaways

- Weather Data Collecting wasted time, minor impact
- How many pumps nearby in radius 10km works surprisingly well
- City Nearby & Distance are ok
- KNN features work too good to be true, what is going on. (Data leakage? k=20... Hmmm)
- mlogloss, [Multi Class LogLoss](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.log_loss.html) of around 0.46 on test set was achieved

### 3. Calculating Optimal route for Maintanance Person

[03 - Optimalisation - find the best Maintanance Route .ipynb](notebooks/03 - Optimalisation - find the best Maintanance Route .ipynb) contains the optimisation notebook

![trp](images/trp.gif)

#### Key Takeaways

- Route optimisation, but that is a first version
- What should be the strategy?
	* Downtime optimisation ?
	* Resource shortage problem ?
	* Cost problem ?
	* Combination ?
- Maintanance is a optimisation problem !

## Part 2 - FastAPI

I have used [fastapi](https://fastapi.tiangolo.com/) to create a MVP of a deployment.

## Instructions

With the default python docker images;

- `docker run -p 8000:8000 -v $(pwd):/vol -it python bash`
- `cd /vol/ && pip install -r requirements.txt`
- `uvicorn main:app --reload --host 0.0.0.0` to start the app

Go to http://127.0.0.1:8000/docs to read the docs and try the api

![pipeline](images/deploy.jpeg)
