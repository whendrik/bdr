from typing import Optional

from fastapi import FastAPI, HTTPException

import pandas as pd
import numpy as np
import frigidum

import trp

app = FastAPI()


@app.get("/")
def read_root():
	return {"Status": "Online - read the /docs for instructions"}

@app.get("/trp_route/{ward}")
def calculate_trp_route(ward: int):

	pumps = pd.read_csv('trp.csv')

	ward = pumps[ (pumps.ward==ward) & (pumps.predict_needs_repair) ]

	if len(ward) < 1:
		raise HTTPException(status_code=404, detail="Ward not Found")

	nodes = ward[ ['latitude', 'longitude'] ].values
	nodes_count = nodes.shape[0]

	trp.dist_sq = np.sum((nodes[:, np.newaxis, :] - nodes[np.newaxis, :, :]) ** 2, axis = -1)
	trp.dist_eu = np.sqrt(trp.dist_sq)
	trp.nodes_count = nodes_count

	local_opt = frigidum.sa(random_start=trp.random_start,
			   objective_function=trp.objective_function,
			   neighbours=[trp.euclidian_bomb_and_fix, trp.euclidian_nuke_and_fix, trp.route_bomb_and_fix, trp.route_nuke_and_fix, trp.random_disconnect_vertices_and_fix],
			   copy_state=frigidum.annealing.naked,
			   T_start=250,
			   alpha=.9,
			   T_stop=0.0001,
			   repeats=1,
			   post_annealing = trp.local_search_2opt)


	return {"route": [int(i) for i in list(local_opt[0])], "total_distance": local_opt[1]}

